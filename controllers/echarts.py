from odoo import http
import json
import random
from odoo.http import request
from pyecharts.charts import Bar
from pyecharts import options as opts
from pyecharts.charts import Line
from pyecharts.charts import Pie
from pyecharts.charts import Gauge
from pyecharts.charts import HeatMap
from pyecharts.faker import Faker

from pyecharts.components import Image
from pyecharts.options import ComponentTitleOpts

from pyecharts.commons.utils import JsCode

import logging

_logger = logging.getLogger(__name__)


class Demo(http.Controller):
    @http.route('/demo', auth='public', type='http', cors='*', methods=['POST', 'GET'], csrf=False)
    def demo(self):
        return json.dumps({
            'bar1': json.loads(line_base()),
            'bar2': json.loads(gauge_base()),
        })


class PyEcharts(http.Controller):
    @http.route('/pyecharts', auth='public', type='http', cors='*', methods=['POST', 'GET'], csrf=False)
    def pyecharts(self, model=None, etype=None):
        """
        {
            'column': 2 / 1,
            'detail': {
                sequence: {
                    'etype': etype,
                    'edata': edata,
                }
            }
        }
        :return:
        """
        # 1、get default dashboard
        dashboard_obj = request.env['echarts.dashboard'].p_get_default_dashboard()
        dashboard_dict = {
            'column': dashboard_obj.column,
            'count': len(dashboard_obj.line_ids),
            'theme': dashboard_obj.theme,
            'height': dashboard_obj.height,
            'is_notice': dashboard_obj.is_notice,
            'notice': [],
            'details': [],
        }
        for line in dashboard_obj.line_ids:
            dashboard_dict['details'].append({
                'sequence': str(line.sequence),
                'etype': line.echart.etype,
                'edata': _get_chart(label=line.echart.etype),
            })
        # Notice
        notices = request.env['echarts.notice'].search([('is_valid', '=', True)], limit=1)
        if dashboard_obj.is_notice and notices:
            for notice in notices:
                # TODO：显示多条排序处理
                dashboard_dict['notice'].append({
                    'title': notice.title,
                    'content': notice.content,
                    'ntype': notice.ntype,
                })
        dashboard_dict['echarts_data'] = _get_chart(label=etype)
        return json.dumps(dashboard_dict)


def _get_chart(label=None):
    if label == 'Bar':
        return json.loads(bar_base())
    elif label == 'Pie':
        return json.loads(pie_base())
    elif label == 'Line':
        return json.loads(line_base())
    elif label == 'Gauge':
        return json.loads(gauge_base())
    elif label == 'Heatmap':
        return json.loads(heatmap_base())
    elif label == 'Image':
        return image_base()
    elif label == 'MutiplePie':
        return mutiple_pie_base()
    else:
        return {}


fn = """
    function(params) {
        if(params.name == '其他')
            return '\\n\\n\\n' + params.name + ' : ' + params.value + '%';
        return params.name + ' : ' + params.value + '%';
    }
    """


def mutiple_pie_base():
    # c = opts.LabelOpts(formatter=JsCode(fn), position="center")
    # c.opts['formatter'] = fn
    # opts_result = c.opts
    return json_result


def image_base():
    image = Image()
    img_src = (
        "https://user-images.githubusercontent.com/19553554/"
        "71825144-2d568180-30d6-11ea-8ee0-63c849cfd934.png"
    )
    image.add(
        src=img_src,
        style_opts={"width": "200px", "height": "200px", "style": "margin-top: 20px"},
    )
    image.set_global_opts(
        title_opts=ComponentTitleOpts(title="Image-基本示例", subtitle="我是副标题支持换行哦")
    )
    return image.html_content.split('"')[1]


def bar_base():
    c = (
        Bar()
            .add_xaxis(["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"])
            .add_yaxis("商家A", [random.randint(0, 100) for _ in range(6)])
            .add_yaxis("商家B", [random.randint(0, 100) for _ in range(6)])
            .set_global_opts(
            title_opts=opts.TitleOpts(title="Title", subtitle="Subtitle"),
            toolbox_opts=opts.ToolboxOpts(is_show=True, pos_left='90%', pos_top='top',
                                          feature={'saveAsImage': {}, 'dataView': {}}),
        )
            .dump_options_with_quotes()
    )
    return c


def pie_base():
    c = (
        Pie()
            .add("", [list(z) for z in zip(Faker.choose(), Faker.values())])
            .set_global_opts(title_opts=opts.TitleOpts(title="Pie-基本示例"))
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}"))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="Bar-显示 ToolBox"),
            toolbox_opts=opts.ToolboxOpts(is_show=True),
            legend_opts=opts.LegendOpts(is_show=True),
        )
            .dump_options_with_quotes()
    )
    return c


def line_base():
    x_data = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    y_data = [820, 932, 901, 934, 1290, 1330, 1320]

    c = (
        Line()
            .set_global_opts(
            tooltip_opts=opts.TooltipOpts(is_show=True),
            toolbox_opts=opts.ToolboxOpts(is_show=True, pos_left='90%', pos_top='top'),
            xaxis_opts=opts.AxisOpts(type_="category"),
            yaxis_opts=opts.AxisOpts(
                type_="value",
                axistick_opts=opts.AxisTickOpts(is_show=True),
                splitline_opts=opts.SplitLineOpts(is_show=True),
            ),
        )
            .add_xaxis(xaxis_data=x_data)
            .add_yaxis(
            series_name="",
            y_axis=y_data,
            symbol="emptyCircle",
            is_symbol_show=True,
            label_opts=opts.LabelOpts(is_show=True),
            markline_opts=opts.MarkLineOpts(data=[opts.MarkLineItem(type_="average")]),
        )
            .dump_options_with_quotes()
    )
    return c


def gauge_base():
    c = (
        Gauge()
            .add(
            "业务指标",
            [("完成率", 55.5)],
            axisline_opts=opts.AxisLineOpts(
                linestyle_opts=opts.LineStyleOpts(
                    color=[(0.3, "#67e0e3"), (0.7, "#37a2da"), (1, "#fd666d")], width=30
                )
            ),
        )
            .set_global_opts(
            # title_opts=opts.TitleOpts(title="Gauge-不同颜色"),
            legend_opts=opts.LegendOpts(is_show=False),
        ).dump_options_with_quotes()
    )
    return c


def heatmap_base():
    value = [[i, j, random.randint(0, 50)] for i in range(24) for j in range(7)]
    c = (
        HeatMap()
            .add_xaxis(Faker.clock)
            .add_yaxis("series0", Faker.week, value)
            .set_global_opts(
            # title_opts=opts.TitleOpts(title="HeatMap-基本示例"),
            visualmap_opts=opts.VisualMapOpts(),
        ).dump_options_with_quotes()
    )
    return c


json_result = {
    "animation": True,
    "animationThreshold": 2000,
    "animationDuration": 1000,
    "animationEasing": "cubicOut",
    "animationDelay": 0,
    "animationDurationUpdate": 300,
    "animationEasingUpdate": "cubicOut",
    "animationDelayUpdate": 0,
    "color": [
        "#c23531",
        "#2f4554",
        "#61a0a8",
        "#d48265",
        "#749f83",
        "#ca8622",
        "#bda29a",
        "#6e7074",
        "#546570",
        "#c4ccd3",
        "#f05b72",
        "#ef5b9c",
        "#f47920",
        "#905a3d",
        "#fab27b",
        "#2a5caa",
        "#444693",
        "#726930",
        "#b2d235",
        "#6d8346",
        "#ac6767",
        "#1d953f",
        "#6950a1",
        "#918597"
    ],
    "series": [
        {
            "type": "pie",
            "clockwise": True,
            "data": [
                {
                    "name": "\u5267\u60c5",
                    "value": 25
                },
                {
                    "name": "\u5176\u4ed6",
                    "value": 75
                }
            ],
            "radius": [
                60,
                80
            ],
            "center": [
                "20%",
                "30%"
            ],
            "label": {
                "show": True,
                "position": "center",
                "margin": 8,
                "formatter": "\u5267\u60c5 ：25%\n其他 ：75%"
            }
        },
        {
            "type": "pie",
            "clockwise": True,
            "data": [
                {
                    "name": "\u5947\u5e7b",
                    "value": 24
                },
                {
                    "name": "\u5176\u4ed6",
                    "value": 76
                }
            ],
            "radius": [
                60,
                80
            ],
            "center": [
                "55%",
                "30%"
            ],
            "label": {
                "show": True,
                "position": "center",
                "margin": 8,
                "formatter": "\u5947\u5e7b ：24%\n其他 ：76%"
            }
        },
        {
            "type": "pie",
            "clockwise": True,
            "data": [
                {
                    "name": "\u7231\u60c5",
                    "value": 14
                },
                {
                    "name": "\u5176\u4ed6",
                    "value": 86
                }
            ],
            "radius": [
                60,
                80
            ],
            "center": [
                "20%",
                "70%"
            ],
            "label": {
                "show": True,
                "position": "center",
                "margin": 8,
                "formatter": "\u7231\u60c5 ：14%\n其他 ：86%"
            }
        },
        {
            "type": "pie",
            "clockwise": True,
            "data": [
                {
                    "name": "\u60ca\u609a",
                    "value": 11
                },
                {
                    "name": "\u5176\u4ed6",
                    "value": 89
                }
            ],
            "radius": [
                60,
                80
            ],
            "center": [
                "55%",
                "70%"
            ],
            "label": {
                "show": True,
                "position": "center",
                "margin": 8,
                "formatter": "\u60ca\u609a ：11%\n其他 ：89%"
            }
        }
    ],
    "legend": [
        {
            "data": [
                "\u5267\u60c5",
                "\u5176\u4ed6",
                "\u5947\u5e7b",
                "\u7231\u60c5",
                "\u60ca\u609a"
            ],
            "selected": {},
            "type": "scroll",
            "show": True,
            "left": "80%",
            "top": "20%",
            "orient": "vertical",
            "padding": 5,
            "itemGap": 10,
            "itemWidth": 25,
            "itemHeight": 14
        }
    ],
    "tooltip": {
        "show": True,
        "trigger": "item",
        "triggerOn": "mousemove|click",
        "axisPointer": {
            "type": "line"
        },
        "showContent": True,
        "alwaysShowContent": False,
        "showDelay": 0,
        "hideDelay": 100,
        "textStyle": {
            "fontSize": 14
        },
        "borderWidth": 0,
        "padding": 5
    },
    "title": [
        {
            "text": "Pie-\u591a\u997c\u56fe\u57fa\u672c\u793a\u4f8b",
            "padding": 5,
            "itemGap": 10
        }
    ]
}
